#include "KassouUserClient.h"
#include <IOKit/IOLib.h>
#include <IOKit/assert.h>

#define super IOUserClient

OSDefineMetaClassAndStructors(KassouUserClientClassName, IOUserClient);

bool KassouUserClientClassName::initWithTask(task_t owningTask, void* securityID,
											 UInt32 type, OSDictionary* properties)
{
	bool success;
	
	success = super::initWithTask(owningTask, securityID, type, properties);
	
	// Can't call getName() until init() has been called.
	IOLog("%s[%p]::%s(type = " UInt32_FORMAT ")\n", getName(), this, __FUNCTION__, type);
	
	fTask = owningTask;
	fDriver = NULL;
	
	return success;
}

bool KassouUserClientClassName::start(IOService* provider)
{
	IOLog("%s[%p]::%s(provider = %p)\n", getName(), this, __FUNCTION__, provider);
	
	if (!super::start(provider))
		return false;
	
	/*
	 * Our provider should be a Kassou object. Verify that before proceeding.
	 */
	
	assert(OSDynamicCast(KassouClassName, provider));
	fDriver = (KassouClassName*) provider;
	
	/*
	 * Set up some memory to be shared between this user client instance and its
	 * client process. The client will call in to map this memory, and I/O Kit
	 * will call clientMemoryForType to obtain this memory descriptor.
	 */
	
	fClientSharedMemory = IOBufferMemoryDescriptor::withOptions(kIOMemoryKernelUserShared, sizeof(SampleSharedMemory));
	if (!fClientSharedMemory)
		return false;
	
	fClientShared = (SampleSharedMemory *) fClientSharedMemory->getBytesNoCopy();
	
	fClientShared->field1 = 0x11111111; // same in all endianesses...
	fClientShared->field2 = 0x22222222; // ditto
	fClientShared->field3 = 0x33333333; // ditto
	
	(void)strncpy(fClientShared->string, "some data", sizeof(fClientShared->string) - 1);
	fClientShared->string[sizeof(fClientShared->string) - 1] = '\0';
	fOpenCount = 1;
	
	return true;
}


IOReturn KassouUserClientClassName::clientClose(void)
{
	IOLog("%s[%p]::%s()\n", getName(), this, __FUNCTION__);
	
	if( !isInactive())
		terminate();
	
	return kIOReturnSuccess;
}

void KassouUserClientClassName::stop(IOService* provider)
{
	IOLog("%s[%p]::%s(provider = %p)\n", getName(), this, __FUNCTION__, provider);
	
	if (fClientSharedMemory) {
		fClientSharedMemory->release();
		fClientSharedMemory = 0;
	}
	
	super::stop(provider);
}

/*
 * Look up the external methods - supply a description of the parameters 
 * available to be called 
 *
 * This is the old way which only supports 32-bit user processes.
 */
IOExternalMethod* KassouUserClientClassName::getTargetAndMethodForIndex(IOService** targetP, UInt32 index)
{
  // 
  static const IOExternalMethod methodDescs[kKassouUserClientNumMethods] = {
    { NULL, (IOMethod) &KassouUserClientClassName::GetStatusReg,
      kIOUCScalarIScalarO, 0, 1}, // no sclaars in, 1 scalar out.
     { NULL, (IOMethod) &KassouUserClientClassName::WriteRegister,
      kIOUCScalarIScalarO, 2, 0}, // 2 scalars in, no scalars out
     { NULL, (IOMethod) &KassouUserClientClassName::ReadRegister,
      kIOUCScalarIScalarO, 1, 1}, // 1 scalars in, 1 scalar out
     { NULL, (IOMethod) &KassouUserClientClassName::WriteConfigRegister,
       kIOUCScalarIScalarO, 3, 0}, // 2 scalars in, 0 scalar out
     { NULL, (IOMethod) &KassouUserClientClassName::ReadConfigRegister,
       kIOUCScalarIScalarO, 2, 1}, // 1 scalars in, 1 scalar out
  };
  
  IOLog("%s[%p]::%s(index = "UInt32_FORMAT")\n", getName(), this, __FUNCTION__, index);
	
  *targetP = this;
  if (index < kKassouUserClientNumMethods)
    {
      IOLog("kassou call in range: %d\n", index);
      return (IOExternalMethod*) &methodDescs[index];
    }
  else
    {
      IOLog("kassou call out of range: %d\n", index);
      return NULL;
    }	
  return NULL;
}

// The client wants access to the card's memory.
IOReturn KassouUserClientClassName::clientMemoryForType(UInt32 type, IOOptionBits *options, IOMemoryDescriptor **memory)
{
	IOReturn ret;
	
	IOLog("KassouUserClient::clientMemoryForType(" UInt32_FORMAT ")\n", type);
	
	switch(type)
	{
		case kVoodooMemoryRange:
			*memory = fDriver->copyGlobalMemory();
			ret = kIOReturnSuccess;
			break;
		default:
			ret = kIOReturnBadArgument;
			break;
	}
	return ret;
}

IOReturn KassouUserClientClassName::GetStatusReg(UInt32 *statusWord)
{
	IOLog("KassouUC::GetStatusReg\n");
	
	fDriver->GetStatusReg(statusWord);
	
	IOLog("Status word written\n");
	
	return kIOReturnSuccess;
}

IOReturn KassouUserClientClassName::WriteRegister(UInt32 offset, UInt32 value)
{
	// 1KB of registers. All access must be on word boundaries.
	IOLog("KassouUserClient WriteRegister: Write %08X to offset %X\n", value, offset);
	if((offset % 4) != 0) return kIOReturnBadArgument;
	if(offset > 1024) return kIOReturnBadArgument;
	fDriver->WriteRegister(offset, value);
	return kIOReturnSuccess;
}

IOReturn KassouUserClientClassName::WriteConfigRegister(UInt32 offset, UInt32 sizeInBytes, UInt32 value)
{
  fDriver->WriteConfigRegister(offset, sizeInBytes, value);
  return kIOReturnSuccess;
}

IOReturn KassouUserClientClassName::ReadConfigRegister(UInt32 offset, UInt32 sizeInBytes, UInt32 *value)
{
  fDriver->ReadConfigRegister(offset, sizeInBytes, value);
  return kIOReturnSuccess;
}

IOReturn KassouUserClientClassName::ReadRegister(UInt32 offset, UInt32 *value)
{
	// 1KB of registers. All access must be on word boundaries.
	IOLog("KassouUserClient ReadRegister: Read offset %X\n", offset);
	if((offset % 4) != 0) return kIOReturnBadArgument;
	if(offset > 1024) return kIOReturnBadArgument;
	fDriver->ReadRegister(offset, value);
	return kIOReturnSuccess;
}
