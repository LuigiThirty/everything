#pragma once

typedef struct SampleSharedMemory {
  uint32_t field1;
  uint32_t field2;
  uint32_t field3;
  char string[100];
} SampleSharedMemory;
