#include <IOKit/IOUserClient.h>
#include <IOKit/IOBufferMemoryDescriptor.h>

#include "Kassou.h"
#include "KassouShared.h"

#include "KassouUserClientMethods.h"

#define KassouUserClientClassName com_luigithirty_driver_KassouUserClient

#define kIOUCVariableStructureSize 0xFFFFFFFF

class KassouUserClientClassName : public IOUserClient
{
  OSDeclareDefaultStructors( KassouUserClientClassName );

 private:
  KassouClassName *fDriver;
  IOBufferMemoryDescriptor *fClientSharedMemory;
  SampleSharedMemory *fClientShared;
  task_t fTask;
  int32_t fOpenCount;
  bool fCrossEndian;

 public:
  virtual bool start(IOService *provider);
  virtual void stop(IOService *provider);
  virtual bool initWithTask(task_t owningTask,
			    void *securityID,
			    UInt32 type,
			    OSDictionary *properties);
  virtual IOReturn clientClose(void);
  virtual IOExternalMethod *getTargetAndMethodForIndex(IOService **targetP,
						       UInt32 index);

  virtual IOReturn clientMemoryForType(UInt32 type, IOOptionBits *options, IOMemoryDescriptor **memory);

  // my functions
  IOReturn GetStatusReg(UInt32 *dataOut);
  IOReturn WriteRegister(UInt32 offset, UInt32 value);
  IOReturn WriteConfigRegister(UInt32 offset, UInt32 sizeInBytes, UInt32 value);
  IOReturn ReadConfigRegister(UInt32 offset, UInt32 sizeInBytes, UInt32 *value);
  IOReturn ReadRegister(UInt32 offset, UInt32 *value);
};
