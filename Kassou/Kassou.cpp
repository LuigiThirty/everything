/* add your code here */

#include <IOKit/IOLib.h>
#include <IOKit/pci/IOPCIDevice.h>

#include "3DFX.H"
#include "glide/src/glidesys.h"
#include "incsrc/cvgregs.h"

#include "Kassou.h"

#define KassouClassName com_luigithirty_driver_Kassou

OSDefineMetaClassAndStructors(com_luigithirty_driver_Kassou, IOService)

#define super IOService

UInt32 swizzleWord(UInt32 in)
{
  return	\
    ((in & 0x000000FF)<<24)		\
    +((in & 0x0000FF00)<<8)		\
    +((in & 0x00FF0000)>>8)		\
    +((in & 0xFF000000)>>24);
}


bool com_luigithirty_driver_Kassou::init(OSDictionary *dict)
{
  bool result = super::init(dict);
  IOLog("Initializing...\n");

  sstRegs = NULL;
  virtualBase = NULL;
  mem = NULL;
  map = NULL;
  
  return result;
}

void com_luigithirty_driver_Kassou::free(void)
{
	IOLog("Freeing...\n");
	super::free();
}

IOService *com_luigithirty_driver_Kassou::probe(IOService *provider, SInt32 *score)
{
	IOService *result = super::probe(provider, score);
	IOLog("Probing...\n");

	fPCIDevice = OSDynamicCast(IOPCIDevice, provider);
	
	UInt32 revision = fPCIDevice->configRead8(0x08);
	IOLog("Found a 3dfx card, it's a Voodoo %d\n", revision);

	return result;
}

bool com_luigithirty_driver_Kassou::start(IOService *provider)
{
	bool result = super::start(provider);
	IOLog("Starting...\n");

	// Enable memory response.
	fPCIDevice->setMemoryEnable(true);

	mem = fPCIDevice->getDeviceMemoryWithRegister(kIOPCIConfigBaseAddress0);
	if (mem != NULL) {
	  IOLog("Range %x: 0x%08X length %08X\n", kIOPCIConfigBaseAddress0,
		mem->getPhysicalAddress(), mem->getLength());
	}

	map = fPCIDevice->mapDeviceMemoryWithRegister(kIOPCIConfigBaseAddress0);
	if (map != NULL) {
	  IOLog("Range@0x%x (" PhysAddr_FORMAT ") mapped to kernel virtual address " VirtAddr_FORMAT "\n",
		kIOPCIConfigBaseAddress0,
		map->getPhysicalAddress(),
		map->getVirtualAddress()
		);
	}

	virtualBase = map->getVirtualAddress();
	sstRegs = (SstRegs *)virtualBase;
	
	fPCIDevice->configWrite32(0x40, 1); // Enable write to fbiInit registers

	IOLog("Registering Voodoo driver service...\n");
	registerService();

	return result;
}

void com_luigithirty_driver_Kassou::stop(IOService *provider)
{
  IOLog("Stopping...\n");
  super::stop(provider);
  
  if(map != NULL) map->release();
}

/*
 * Method to supply an IOMemoryDescriptor for the user client to map into
 * the client process. This sample just supplies all of the hardware memory
 * associated with the PCI device's Base Address Register 0.
 * In a real driver mapping hardware memory would only ever be used in some
 * limited high performance scenarios where the device range can be safely
 * accessed by client code with compromising system stability.
 */

IOMemoryDescriptor * KassouClassName::copyGlobalMemory( void )
{
  IOMemoryDescriptor* memory;

  // 16MB of linear addressing for the Voodoo2.
  memory = fPCIDevice->getDeviceMemoryWithRegister( kIOPCIConfigBaseAddress0 );
  if(memory)
    memory->retain();

  return memory;
}

// UserClient methods here.
IOReturn KassouClassName::GetStatusReg(UInt32 *statusWord)
{
  IOLog("Kassou::GetStatusReg\n");

  OSSynchronizeIO();
  FxU32 lfbMode = swizzleWord(sstRegs->lfbMode);
  lfbMode |= (1<<11)|(1<<12)|(1<<15)|(1<<16);
  sstRegs->lfbMode = swizzleWord(lfbMode);

  *statusWord = swizzleWord(sstRegs->status);
  return kIOReturnSuccess;
}

IOReturn KassouClassName::WriteRegister(UInt32 offset, UInt32 value)
{
  IOLog("Kassou::WriteRegister: Write %08X to offset %X\n", value, offset);
  uint32_t *regToWrite = (uint32_t *)(virtualBase+VOODOO_MMREG_OFFSET+offset);
  *regToWrite = swizzleWord(value);
  OSSynchronizeIO();
  return kIOReturnSuccess;
}

IOReturn KassouClassName::WriteConfigRegister(UInt32 offset, UInt32 sizeInBytes, UInt32 value)
{
  switch(sizeInBytes)
    {
    case 4:
      fPCIDevice->configWrite32(offset, value);
	  OSSynchronizeIO();
      return kIOReturnSuccess;
      break;
    case 3:
      fPCIDevice->configWrite8(offset+0, (value & 0x0000FF));
      fPCIDevice->configWrite8(offset+1, (value & 0x00FF00) >> 8);
      fPCIDevice->configWrite8(offset+2, (value & 0xFF0000) >> 16);
	  OSSynchronizeIO();
      return kIOReturnSuccess;
      break;
    case 2:
      fPCIDevice->configWrite16(offset, value);
	  OSSynchronizeIO();
      return kIOReturnSuccess;
      break;
    case 1:
      fPCIDevice->configWrite8(offset, value);
	  OSSynchronizeIO();
      return kIOReturnSuccess;
      break;      
    }

  return kIOReturnBadArgument;
}

IOReturn KassouClassName::ReadConfigRegister(UInt32 offset, UInt32 sizeInBytes, UInt32 *value)
{
  uint8_t valHi, valMid, valLo;

  switch(sizeInBytes)
    {
    case 4:
		OSSynchronizeIO();
      *value = fPCIDevice->configRead32(offset);
      return kIOReturnSuccess;
      break;
    case 3:
		OSSynchronizeIO();
      valLo = fPCIDevice->configRead8(offset);
      valMid = fPCIDevice->configRead8(offset+1);
      valHi = fPCIDevice->configRead8(offset+2);
      *value = (valHi << 16) | (valMid << 8) | (valLo);
      return kIOReturnSuccess;
      break;
    case 2:
		OSSynchronizeIO();
      *value = fPCIDevice->configRead16(offset);
      return kIOReturnSuccess;
      break;
    case 1:
		OSSynchronizeIO();
      *value = fPCIDevice->configRead8(offset);
      return kIOReturnSuccess;
      break;
    }

  return kIOReturnBadArgument;
}

IOReturn KassouClassName::ReadRegister(UInt32 offset, UInt32 *value)
{
  OSSynchronizeIO();
  uint32_t *regToRead = (uint32_t *)(virtualBase+VOODOO_MMREG_OFFSET+offset);
  *value = swizzleWord(*regToRead);
  return kIOReturnSuccess;
}
