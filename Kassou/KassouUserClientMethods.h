#pragma once

typedef enum KassouUserClientMethods {
  kKassouUserClientGetStatusRegister,
  kKassouUCWriteRegister,		
  kKassouUCReadRegister,
  kKassouUCWriteConfigRegister,
  kKassouUCReadConfigRegister,
  kKassouUserClientNumMethods
} KassouUserClientMethods;

enum {
  kVoodooMemoryRange = 100
};
