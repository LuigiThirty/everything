/* add your code here */

#include <IOKit/IOService.h>
#include <IOKit/pci/IOPCIDevice.h>
#include "KassouShared.h"

#define UInt32_FORMAT    "%u"
#define UInt32_x_FORMAT  "0x%08x"
#define PhysAddr_FORMAT  "0x%08x"
#define PhysLen_FORMAT   "%u"
#define VirtAddr_FORMAT  "0x%08x"
#define ByteCount_FORMAT "%u"

// 16MB linear address space.
#define VOODOO_MEMMAP_SIZE  0x1000000
#define VOODOO_MMREG_OFFSET 0x0
#define VOODOO_LFB_OFFSET   0x400000
#define VOODOO_TMEM_OFFSET  0x800000

#include "3DFX.H"
#include "glide/src/glidesys.h"
#include "incsrc/cvgregs.h"

#define KassouClassName com_luigithirty_driver_Kassou

class IOMemoryDescriptor;

class com_luigithirty_driver_Kassou : public IOService
{
	OSDeclareDefaultStructors(com_luigithirty_driver_Kassou)
	
private:
	IOPCIDevice *fPCIDevice;
	
	IOMemoryDescriptor *mem;
	IOMemoryMap        *map;
	
	IOVirtualAddress virtualBase; // Base address of virtual region.
	SstRegs *sstRegs;             // SST register structure.
	
public:
		virtual bool init(OSDictionary *dictionary = 0);
	virtual void free(void);
	virtual IOService *probe(IOService *provider, SInt32 *score);
	virtual bool start(IOService *provider);
	virtual void stop(IOService *provider);
	
	virtual IOReturn newUserClient(task_t owningTask, void *securityID, UInt32 type, OSDictionary *properties, IOUserClient **handler);
	virtual IOMemoryDescriptor *copyGlobalMemory(void);
	
	// Kernel methods for the UserClient.
	virtual IOReturn GetStatusReg(UInt32 *statusWord);
	virtual IOReturn ReadRegister(UInt32 offset, UInt32 *value);
	virtual IOReturn WriteConfigRegister(UInt32 offset, UInt32 sizeInBytes, UInt32 value);
	virtual IOReturn ReadConfigRegister(UInt32 offset, UInt32 sizeInBytes, UInt32 *value);
	virtual IOReturn WriteRegister(UInt32 offset, UInt32 value);		
};
