/*
 *  kassou-dylib.h
 *  kassou-dylib
 *
 *  Created by Kate on 6/19/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef kassou_dylib_
#define kassou_dylib_

#include <glide.h>

/* The classes below are exported */
#pragma GCC visibility push(default)

class kassou_dylib
{
	public:
		void HelloWorld(const char *);
		
		// Glide stuff
		FxBool grSstQueryBoards(GrHwConfiguration *hwConfig);
};

#pragma GCC visibility pop
#endif
