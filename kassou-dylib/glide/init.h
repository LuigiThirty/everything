/*
 *  init.h
 *  KassouTool
 *
 *  Created by Kate on 6/19/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */
 
#pragma once

#include <glide.h>
#include "../Kassou/KassouUserClientMethods.h"
#include "pci.h"

#include <cvgdefs.h>
#include <cvgregs.h>
#include <cvginfo.h>
#include "cvg/init/sst1init.h"

extern "C"
{
	UInt32 swizzleWord(UInt32 in);
	UInt16 swizzle16(UInt16 in);
	
	void InitMacEnvironment(void);
}

void SetPixel(uint16_t x, uint16_t y, uint16_t data);

extern vm_address_t voodooAddr;
extern vm_size_t voodooSize;

extern struct GrGC_s GC;
