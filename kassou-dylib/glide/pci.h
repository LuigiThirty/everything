/*
 *  pci.h
 *  KassouTool
 *
 *  Created by Kate on 6/19/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <IOKit/IOKitLib.h>
#include <CoreFoundation/CoreFoundation.h>
#include <glide.h>
#include <fxpci.h>

#include "../Kassou/KassouUserClientMethods.h"

extern io_service_t voodoo_service;
extern io_connect_t voodoo_connection;
extern int numSsts;

typedef struct CardInfo
{
	GrSstType sstType;
	// TODO: more...
};

extern CardInfo sstCardInfo[4];

void getProvider();
io_connect_t ConnectToVoodoo2(io_service_t service);

FxU32 HowManyVoodoo2s();

FxBool PCICFG_WR(PciRegister configRegister, uint32_t value);
FxBool PCICFG_RD(PciRegister configRegister, FxU32 *value);

