/*
 *  pci.cpp
 *  KassouTool
 *
 *  Created by Kate on 6/19/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */

#include "pci.h"

io_service_t voodoo_service = 0;	// for card 0
io_connect_t voodoo_connection = 0;	// for card 0
int numSsts = 0;
CardInfo sstCardInfo[4];

FxBool PCICFG_WR(PciRegister configRegister, uint32_t value)
{
  assert(IOConnectMethodScalarIScalarO(voodoo_connection, kKassouUCWriteConfigRegister,
					3, // 2 scalar inputs
					0, // 0 scalar output
					configRegister.regAddress,
					configRegister.sizeInBytes,
					value) == 0);
					return FXTRUE;
}

FxBool PCICFG_RD(PciRegister configRegister, FxU32 *value)
{
  assert(IOConnectMethodScalarIScalarO(voodoo_connection, kKassouUCReadConfigRegister,
                                        2, // 2 scalar inputs
                                        1, // 0 scalar output
                                        configRegister.regAddress,
					configRegister.sizeInBytes,
					value) == 0);
					return FXTRUE;
}


void getProvider()
{
	mach_port_t myMasterPort;
	kern_return_t kr;
	
	// Create a dictionary for matching with our driver.
	io_iterator_t iter;
	
	// Get the master port and use that to match the Voodoo driver.
	kr = IOMasterPort(MACH_PORT_NULL, &myMasterPort);
	kr = IOServiceGetMatchingServices(myMasterPort, IOServiceMatching("com_luigithirty_driver_Kassou"), &iter); 
	assert(KERN_SUCCESS == kr);
	
	for(;
		(voodoo_service = IOIteratorNext(iter));
		IOObjectRelease(voodoo_service))
    {
		numSsts++;
		sstCardInfo[numSsts-1].sstType = GR_SSTTYPE_Voodoo2; // we know this from the PCI info
		io_string_t path;
		kr = IORegistryEntryGetPath(voodoo_service, kIOServicePlane, path);
		printf("Found a device of class: %s\n", path);
		voodoo_connection = ConnectToVoodoo2(voodoo_service);
    }
}

FxU32 HowManyVoodoo2s()
{
	kern_return_t kr;
	mach_port_t myMasterPort;
	io_iterator_t iter;
	int count = 0;
	
	kr = IOMasterPort(MACH_PORT_NULL, &myMasterPort);
	kr = IOServiceGetMatchingServices(myMasterPort, IOServiceMatching("com_luigithirty_driver_Kassou"), &iter); 
	assert(KERN_SUCCESS == kr);
	
	for(;
		(voodoo_service = IOIteratorNext(iter));
		IOObjectRelease(voodoo_service))
    {
		count++;
    }
	
	return count;
}

io_connect_t ConnectToVoodoo2(io_service_t service)
{
	io_connect_t connect = 0;
	
	kern_return_t kr = IOServiceOpen(service, mach_task_self(), 0, &connect);
	if ( kr != KERN_SUCCESS ){
		fprintf( stderr, "IOServiceOpen failed 0x%x\n", kr );
		exit( 1 );
	}
	
	printf("Connected to Voodoo2 driver!\n");
	
	return connect;
}

const PciRegister PCI_VENDOR_ID       = { 0x0,  2, READ_ONLY };
const PciRegister PCI_DEVICE_ID       = { 0x2,  2, READ_ONLY };
const PciRegister PCI_COMMAND         = { 0x4,  2, READ_WRITE };
const PciRegister PCI_STATUS          = { 0x6,  2, READ_WRITE };
const PciRegister PCI_REVISION_ID     = { 0x8,  1, READ_ONLY };
const PciRegister PCI_CLASS_CODE      = { 0x9,  3, READ_ONLY };
const PciRegister PCI_CACHE_LINE_SIZE = { 0xC,  1, READ_WRITE };
const PciRegister PCI_LATENCY_TIMER   = { 0xD,  1, READ_WRITE };
const PciRegister PCI_HEADER_TYPE     = { 0xE,  1, READ_ONLY };
const PciRegister PCI_BIST            = { 0xF,  1, READ_WRITE };
const PciRegister PCI_BASE_ADDRESS_0  = { 0x10, 4, READ_WRITE };
const PciRegister PCI_BASE_ADDRESS_1  = { 0x14, 4, READ_WRITE };
const PciRegister PCI_IO_BASE_ADDRESS = { 0x18, 4, READ_WRITE };
const PciRegister PCI_SUBVENDOR_ID    = { 0x2C, 4, READ_ONLY };
const PciRegister PCI_SUBSYSTEM_ID    = { 0x2E, 4, READ_ONLY };
const PciRegister PCI_ROM_BASE_ADDRESS= { 0x30, 4, READ_WRITE };
const PciRegister PCI_CAP_PTR         = { 0x34, 4, READ_WRITE };
const PciRegister PCI_INTERRUPT_LINE  = { 0x3C, 1, READ_WRITE };
const PciRegister PCI_INTERRUPT_PIN   = { 0x3D, 1, READ_ONLY };
const PciRegister PCI_MIN_GNT         = { 0x3E, 1, READ_ONLY };
const PciRegister PCI_MAX_LAT         = { 0x3F, 1, READ_ONLY };
const PciRegister PCI_FAB_ID          = { 0x40, 1, READ_ONLY };
const PciRegister PCI_CONFIG_STATUS   = { 0x4C, 4, READ_WRITE };
const PciRegister PCI_CONFIG_SCRATCH  = { 0x50, 4, READ_WRITE };
const PciRegister PCI_AGP_CAP_ID      = { 0x54, 4, READ_ONLY };
const PciRegister PCI_AGP_STATUS      = { 0x58, 4, READ_ONLY };
const PciRegister PCI_AGP_CMD         = { 0x5C, 4, READ_WRITE };
const PciRegister PCI_ACPI_CAP_ID     = { 0x60, 4, READ_ONLY };
const PciRegister PCI_CNTRL_STATUS    = { 0x64, 4, READ_WRITE };

/* sst1 definitions left in for compatability */
const PciRegister PCI_SST1_INIT_ENABLE = { 0x40, 4, READ_WRITE }; 
const PciRegister PCI_SST1_BUS_SNOOP_0 = { 0x44, 4, READ_WRITE }; 
const PciRegister PCI_SST1_BUS_SNOOP_1 = { 0x48, 4, READ_WRITE }; 
const PciRegister PCI_SST1_CFG_STATUS  = { 0x4C, 4, READ_WRITE };

