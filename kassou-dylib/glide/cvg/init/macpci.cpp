/*
 *  macpci.cpp
 *  kassou-dylib
 *
 *  Created by Kate on 6/29/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */

#include "macpci.h"
#include <glide.h>
#include <fxpci.h>
#include <assert.h>
#include <stdio.h>

extern FxBool PCICFG_RD(PciRegister configRegister, FxU32 *value);
extern FxBool PCICFG_WR(PciRegister configRegister, uint32_t value);

FX_ENTRY FxBool FX_CALL 
pciGetConfigData( PciRegister reg, FxU32 device_number, FxU32 *data )
{
	return PCICFG_RD(reg, data);
}

FX_ENTRY FxBool FX_CALL 
pciSetConfigData( PciRegister reg, FxU32 device_number, FxU32 *data )
{
	return PCICFG_WR(reg, *data);
}

FX_ENTRY FxBool FX_CALL
pciOpen( void )
{
	// Always open.
	return FXTRUE;
}

FX_ENTRY FxBool FX_CALL
pciClose( void )
{
	// Always open.
	return FXTRUE;
}

FX_ENTRY FxBool FX_CALL
pciSetMTRR(FxU32 mtrrNo, FxU32 pBaseAddr, FxU32 psz, PciMemType type)
{
	// TODO: What is this trying to do on a PPC?
	return FXTRUE;
}

FX_ENTRY FxBool FX_CALL
pciSetMTRRAmdK6(FxU32 mtrrNo, FxU32 pBaseAddr, FxU32 psz, PciMemType type)
{
	assert(false); // Trap this, we're not a K6
}

FX_ENTRY void   FX_CALL 
pciUnmapPhysical( unsigned long linear_addr, FxU32 length )
{
	// TODO: We should be able to do this in OSX.
	assert(false); // trap this
}

extern FxU32 voodooAddress;

FX_ENTRY FxBool FX_CALL
pciLinearRangeSetPermission(const unsigned long addrBase, const FxU32 addrLen, const FxBool writeableP)
{
	printf("WARNING: Unsupported call to pciLinearRangeSetPermission\n");
	return FXFALSE;
}

// pciMapCardMulti(vendorID, deviceID, sizeOfCard, &sst1InitDeviceNumber, j, 0);
FX_ENTRY FxU32 * FX_CALL pciMapCardMulti(FxU32 vID,FxU32 dID,FxI32 l,FxU32 *dNo,FxU32 cNo,FxU32 aNo)
{
	// Returns the SstBase, so we need the virtual pointer to the card.
	printf("pciMapCardMulti\n");
	
	// Match our Voodoo2.
	if(cNo == 0 && vID == 0x121A && dID == 2)
	{
		return (FxU32 *)voodooAddress;
	}
	
	return NULL;
}

extern FxU32 HowManyVoodoo2s();

FX_ENTRY FxBool FX_CALL 
pciFindCardMulti(FxU32 vID, FxU32 dID, FxU32 *devNum, FxU32 cardNum)
{	
	// Find number of cards that match the vendor and device ID.
	if(HowManyVoodoo2s() > 0 && cardNum == 0)
	{
		// We'll pretend that device 0 is a Voodoo2.
		*devNum = 0;
		return FXTRUE;
	}
	
	return FXFALSE;
}

FX_ENTRY FxBool FX_CALL 
pciDeviceExists( FxU32 device_number )
{
	if(device_number != 0)
	{
		return FXFALSE;
	}
	
	return FXTRUE;
}

FX_ENTRY FxBool FX_CALL
pciFindMTRRMatch(FxU32 pBaseAddrs, FxU32 psz, PciMemType type, FxU32 *mtrrNum)
{
	return FXTRUE; // ???
}

FX_ENTRY FxBool FX_CALL
pciFindFreeMTRR(FxU32 *mtrrNum)
{
	return FXTRUE; // ???
}

FX_ENTRY const char * FX_CALL
pciGetErrorString( void )
{
	// ???
	return "blah";
}

FX_ENTRY FxU32 FX_CALL
pciGetErrorCode( void )
{
	return 0;
}