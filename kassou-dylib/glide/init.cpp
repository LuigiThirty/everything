/*
 *  init.cpp
 *  KassouTool
 *
 *  Created by Kate on 6/19/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */

#include "init.h"
#include "pci.h"
#include <sst1vid.h>

vm_address_t voodooAddress;
vm_size_t voodooSize;

/*
UInt32 swizzleWord(UInt32 in)
{
  return        \
    ((in & 0x000000FF)<<24)             \
    +((in & 0x0000FF00)<<8)             \
    +((in & 0x00FF0000)>>8)             \
    +((in & 0xFF000000)>>24);
}

UInt16 swizzle16(UInt16 in)
{
  return ((in & 0x00FF)<<8) + ((in & 0xFF00)>>8);
}
*/

void getSharedMemory(io_connect_t connect)
{
  kern_return_t kr;

  kr = IOConnectMapMemory(connect, kVoodooMemoryRange, mach_task_self(),
                          &voodooAddress, &voodooSize,
                          kIOMapAnywhere|kIOMapDefaultCache);
  assert(KERN_SUCCESS == kr);

  printf("Client got shared memory pointer at %08X of size %08X\n", voodooAddress, voodooSize);
}

// TODO: Rename to be compatible with sst1init.c
void SetRegister(uint32_t offset, uint32_t value)
{
  __asm("eieio");
  IOConnectMethodScalarIScalarO(voodoo_connection, kKassouUCWriteRegister,
				2, // 2 scalar inputs
				0, // 0 scalar output
				offset, value);
}

uint32_t GetRegister(uint32_t offset)
{
  uint32_t result;
  __asm("eieio");
  IOConnectMethodScalarIScalarO(voodoo_connection, kKassouUCReadRegister,
				1, // 1 scalar input
				1, // 1 scalar output
				offset, &result);
  return result;
}

void SetPixel(uint16_t x, uint16_t y, uint16_t data)
{
  // TODO: Switch based on 16-bit or 32-bit pixel mode.  
  uint16_t *lfbBase = (uint16_t *)(voodooAddress + 0x400000);
  uint32_t lfbOffset = (1024 * y) + x;
  *(lfbBase + lfbOffset) = data;
}

void InitMacEnvironment(void)
{
  // Initialize the SST1, set up state variables, etc.
	
  // Get the driver.
  if(voodoo_connection == 0)
    {
      getProvider();
    }

  getSharedMemory(voodoo_connection);
}
