/*
 *  kassou-dylibPriv.h
 *  kassou-dylib
 *
 *  Created by Kate on 6/19/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class kassou_dylibPriv
{
	public:
		void HelloWorldPriv(const char *);
};

#pragma GCC visibility pop
