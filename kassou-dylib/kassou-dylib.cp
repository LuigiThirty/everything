/*
 *  kassou-dylib.cp
 *  kassou-dylib
 *
 *  Created by Kate on 6/19/20.
 *  Copyright 2020 __MyCompanyName__. All rights reserved.
 *
 */

#include <iostream>
#include "kassou-dylib.h"
#include "kassou-dylibPriv.h"

#include "glide/init.h"
#include "glide/pci.h"

void kassou_dylib::HelloWorld(const char * s)
{
	 kassou_dylibPriv *theObj = new kassou_dylibPriv;
	 theObj->HelloWorldPriv(s);
	 delete theObj;
};

void kassou_dylibPriv::HelloWorldPriv(const char * s) 
{
	std::cout << s << std::endl;
};

