#include <iostream>
#include <IOKit/IOKitLib.h>
#include <CoreFoundation/CoreFoundation.h>

#include "../Kassou/KassouUserClientMethods.h"

#include <glide.h>

#include "kassou-dylib.h"

mach_port_t myMasterPort;
kern_return_t result;
kern_return_t kr;

vm_address_t addr;
vm_size_t size;
