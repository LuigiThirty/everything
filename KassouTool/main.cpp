#include "main.h"

#include "Glide/GlideManager.h"
#include "Simulation/SimulationCore.h"
#include "Math/Math3D.h"

kassou_dylib kassou;

extern void SetPixel(uint16_t x, uint16_t y, uint16_t data);
extern uint32_t GetRegister(uint32_t offset);
extern void SetRegister(uint32_t offset, uint32_t data);


void TestSharedMemory(io_connect_t connect)
{
	kern_return_t kr;
	
	kr = IOConnectMapMemory(connect, kVoodooMemoryRange, mach_task_self(),
							&addr, &size,
							kIOMapAnywhere|kIOMapDefaultCache);
	assert(KERN_SUCCESS == kr);
	
	printf("Client got shared memory pointer at %08X of size %08X\n", addr, size);
}

extern "C"
{
	extern FX_ENTRY FxU32 * FX_CALL sst1InitMapBoard(FxU32);
	FX_ENTRY FxBool FX_CALL sst1InitRegisters(FxU32 *);
	extern void InitMacEnvironment();
}

int main (int argc, char * const argv[]) {
	printf("Userland Voodoo driver program thingy\n");
	
	g_GlideManager.StartGlide();
	g_SimulationCore.SetupSimulation();

	/*
	//Let's start somewhere simple.
	grBufferClear(0, 0, 0);

	// Starfield points are solid white
	grConstantColorValue(0xFFFFFFFF);
	grColorCombine(GR_COMBINE_FUNCTION_LOCAL, GR_COMBINE_FACTOR_NONE,
		GR_COMBINE_LOCAL_CONSTANT, GR_COMBINE_OTHER_NONE, FXFALSE);

	//grDepthBufferMode(GR_DEPTHBUFFER_ZBUFFER);

	GrVertex p[1000];
	// 1000 random points
	for (int n = 0; n < 1000; n++)
	{
		p[n].x = (float)(rand() % 800);
		p[n].y = (float)(rand() % 600);
		p[n].ooz = 65535.0f/65534.0f;	// 65535 / Z
	}
	
	for (int i = 0; i < 1000; i++)
	{
		grDrawPoint(&p[i]);
	}

	grBufferSwap(1);
	*/
	
	g_SimulationCore.SimOneFrame();
	
	return 0;
}

