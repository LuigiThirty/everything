// GlideManager.cpp: implementation of the GlideManager class.
//
//////////////////////////////////////////////////////////////////////

#include "GlideManager.h"
#include "GlideTexture.h"

//
GlideManager g_GlideManager;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

extern "C"
{
	extern void InitMacEnvironment(void);
}

GlideManager::GlideManager()
{
#ifdef __APPLE__
	InitMacEnvironment();
#endif
	grGlideInit();
	grSstQueryHardware(&hwconfig);

	state_stack_position = 0;
	GlideTexture::nextTextureLoadPoint = grTexMinAddress(GR_TMU0);
}

GlideManager::~GlideManager()
{

}

bool GlideManager::StartGlide()
{
	grSstSelect(0);
	printf("Selected Voodoo 0\n");
	assert(grSstWinOpen(NULL, GR_RESOLUTION_800x600, GR_REFRESH_75Hz,
						GR_COLORFORMAT_ARGB, GR_ORIGIN_LOWER_LEFT, 2, 1));
	
	return true;
}

void GlideManager::StopGlide()
{
	grGlideShutdown();
}


void GlideManager::PushState()
{
	assert(state_stack_position < STATE_STACK_SIZE);
	grGlideGetState(&state_stack[state_stack_position]);
	state_stack_position++;
}

void GlideManager::PopState()
{
	assert(state_stack_position > 0);
	state_stack_position--;
	grGlideSetState(&state_stack[state_stack_position]);
}