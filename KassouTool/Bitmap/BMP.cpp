#include "BMP.h"

BMP::BMP(const char *path, GrLfbSrcFmt_t grFormat)
{
	std::fstream fileStream;
	fileStream.open(path, std::ios::in|std::ios::binary);

	fileStream.read((char *)&bfHeader, sizeof(leBITMAPFILEHEADER));
	fileStream.read((char *)&biHeader, sizeof(leBITMAPINFOHEADER));

	pixels = malloc(bfHeader.bfSize.value());
	fileStream.seekg(this->bfHeader.bfOffBits.value());
	fileStream.read((char *)pixels, bfHeader.bfSize.value());

	this->grFormat = grFormat;

	switch(grFormat)
	{
	case GR_LFB_SRC_FMT_565:
		grStride = biHeader.biWidth.value() * 2;
		break;
	default:
		printf("Unknown pixel format specified, don't know the stride of %s!\n", path);
		grStride = biHeader.biWidth.value();
	}
}

BMP::~BMP(void)
{
	if(pixels != NULL) delete pixels;
}

BMP *debugFont;