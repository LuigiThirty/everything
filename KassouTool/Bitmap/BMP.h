#pragma once

#include <glide.h>
#include <fstream>
#include <boost/endian/buffers.hpp>

using namespace boost::endian;

#pragma pack(push, 1)
typedef struct leBITMAPFILEHEADER {
     little_uint16_buf_t	bfType;
     little_uint32_buf_t	bfSize;
     little_uint16_buf_t	bfReserved1;
     little_uint16_buf_t	bfReserved2;
     little_uint32_buf_t	bfOffBits;
} leBITMAPFILEHEADER;

typedef struct leBITMAPINFOHEADER {
      little_uint32_buf_t	biSize;
      little_uint32_buf_t	biWidth;
      little_uint32_buf_t	biHeight;
      little_uint16_buf_t	biPlanes;
      little_uint16_buf_t	biBitCount;
      little_uint32_buf_t	biCompression;
      little_uint32_buf_t	biSizeImage;
      little_uint32_buf_t	biXPelsPerMeter;
      little_uint32_buf_t	biYPelsPerMeter;
      little_uint32_buf_t	biClrUsed;
      little_uint32_buf_t	biClrImportant;
} leBITMAPINFOHEADER;
#pragma pack(pop)
   
class BMP
{
	leBITMAPFILEHEADER bfHeader;
	leBITMAPINFOHEADER biHeader;

	// Glide stuff
	GrLfbSrcFmt_t grFormat;
	FxI32 grStride;

	void *pixels;

public:
	void *GetPixels() { return pixels; }
	FxI32 GetStride() { return grStride; }
	FxI32 GetWidth()  { return biHeader.biWidth.value(); }
	FxI32 GetHeight()  { return biHeader.biHeight.value(); }
	GrLfbSrcFmt_t GetGRFormat() { return grFormat; }

	BMP(const char *path, GrLfbSrcFmt_t grFormat);
	~BMP(void);
};

extern BMP *debugFont;