#pragma once

#include <glide.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include "Bitmap/BMP.h"

// The binary font format of BMFonts.
// https://www.angelcode.com/products/bmfont/doc/file_format.html

#pragma pack(push, 1)
typedef struct
{
	little_int16_buf_t	fontSize;
	little_uint8_buf_t	bitField;
	little_uint8_buf_t	charSet;
	little_uint16_buf_t	stretchH;
	little_uint8_buf_t	aa;
	little_uint8_buf_t	paddingUp;
	little_uint8_buf_t	paddingDown;
	little_uint8_buf_t	paddingLeft;
	little_uint8_buf_t	spacingHoriz;
	little_uint8_buf_t	spacingVert;
	little_uint8_buf_t	outline;
	char	fontName[64];
} BMFontInfoBlock;

typedef struct
{
	little_uint16_buf_t	lineHeight;
	little_uint16_buf_t	base;
	little_uint16_buf_t	scaleW;
	little_uint16_buf_t	scaleH;
	little_uint16_buf_t	pages;
	little_uint8_buf_t	bitField;
	little_uint8_buf_t	alphaChnl;
	little_uint8_buf_t	redChnl;
	little_uint8_buf_t	greenChnl;
	little_uint8_buf_t	blueChnl;
} BMFontCommonBlock;

typedef struct
{
	little_uint32_buf_t	id;
	little_uint16_buf_t	x;
	little_uint16_buf_t	y;
	little_uint16_buf_t	width;
	little_uint16_buf_t	height;
	little_int16_buf_t	xoffset;
	little_int16_buf_t	yoffset;
	little_int16_buf_t	xadvance;
	little_uint8_buf_t	page;
	little_uint8_buf_t	chnl;
} BMFontCharsBlock;

typedef struct
{
	little_uint32_buf_t	first;
	little_uint32_buf_t	second;
	little_int16_buf_t	amount;
} BMFontKerningBlock;
#pragma pack(pop)

class BMFont
{
private:
	void LoadCharsBlock(BMFontCharsBlock *data, FxU32 length);
	void LoadKerningBlock(BMFontKerningBlock *data, FxU32 length);

public:
	BMP *bitmap;

	BMFontInfoBlock infoBlock;

	BMFontCommonBlock commonBlock;

	BMFontCharsBlock *charsBlocks;
	int numCharBlocks;

	BMFontKerningBlock *kerningBlocks;
	int numKerningBlocks;

	BMFont(const char *path, const char *bmp_path);
	~BMFont(void);

	BMFontCharsBlock *GetCharBlock(char c) { return &charsBlocks[c]; }

	void PutString(const char *str, int dest_x, int dest_y);
	FxU16 BlitChar(char c, int dest_x, int dest_y);
};

extern BMFont *bmFontMonogram32;
extern BMFont *bmFontMonogram16;